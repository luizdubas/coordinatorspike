import UIKit
import SwiftUI

protocol Coordinator {
    var context: UINavigationController { get }
    
    func start()
}

extension Coordinator {
    func presentReplacing<Content>(view: Content) where Content: View {
        context.setViewControllers([UIHostingController(rootView: view)], animated: true)
    }
    
    func push<Content>(view: Content) where Content: View {
        context.pushViewController(UIHostingController(rootView: view), animated: true)
    }
    
    func presentModal<Content>(view: Content, presentationStyle: UIModalPresentationStyle = .fullScreen, completion: (() -> ())? = nil) where Content: View {
        let viewController = UIHostingController(rootView: view)
        viewController.modalPresentationStyle = presentationStyle
        context.present(viewController, animated: true, completion: completion)
    }
}

extension UIViewController {
    func leafViewController() -> UIViewController {
        guard let presentedVC = self.presentedViewController else {
            return self
        }
        return presentedVC.leafViewController()
    }
}
