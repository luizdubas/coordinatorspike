import Foundation
import Combine

class AppStateManager {
    let stateSubject = CurrentValueSubject<AppState, Never>(AppState.unauthenticated)
    
    func login() {
        stateSubject.send(.authenticated)
    }
    
    func logout() {
        stateSubject.send(.unauthenticated)
    }
}

enum AppState {
    case unauthenticated
    case authenticated
}
