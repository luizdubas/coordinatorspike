import UIKit
import Combine

class AppCoordinator: Coordinator {
    
    let context: UINavigationController
    let appStateManager: AppStateManager
    var stateSinkCancellable: AnyCancellable?
    var currentCoordinator: Coordinator?
    
    init(context: UINavigationController,
         appStateManager: AppStateManager = .init()) {
        self.context = context
        self.appStateManager = appStateManager
    }
    
    func start() {
        stateSinkCancellable = appStateManager.stateSubject.sink(receiveValue: listenTo)
    }
    
    private func listenTo(state: AppState) {
        currentCoordinator = nil
        switch state {
        case .unauthenticated:
            showFeatureA()
        case .authenticated:
            showFeatureB()
        }
    }
    
    private func showFeatureA() {
        let newCoordinator = LoginCoordinator(context: context,
                                              appStateManager: appStateManager)
        newCoordinator.start()
        currentCoordinator = newCoordinator
    }
    
    private func showFeatureB() {
        let newCoordinator = ChatListCoordinator(context: context,
                                                 appStateManager: appStateManager)
        newCoordinator.start()
        currentCoordinator = newCoordinator
    }
    
}
