import Foundation
import SwiftUI

class LoginCoordinator: Coordinator {
    
    let context: UINavigationController
    let appStateManager: AppStateManager
    
    init(context: UINavigationController,
         appStateManager: AppStateManager = .init()) {
        self.context = context
        self.appStateManager = appStateManager
    }
    
    func start() {
        presentEmailScreen()
    }
    
    func presentEmailScreen() {
        let actionHandler = { [weak self] (action: EmailView.Actions) in
            switch action {
            case .next:
                self?.presentPasswordScreen()
            }
        }
        presentReplacing(view: EmailView(actionHandler: actionHandler))
    }
    
    func presentPasswordScreen() {
        let actionHandler = { [weak self] (action: PasswordView.Actions) in
            switch action {
            case .login:
                self?.appStateManager.login()
            }
        }
        push(view: PasswordView(actionHandler: actionHandler))
    }
}
