import SwiftUI

struct EmailView: View {
    let actionHandler: (Actions) -> Void
    @State var email: String = ""
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Type your email")
                    .font(.title)
                    .padding()
                Spacer()
                TextField("email", text: $email)
                    .textContentType(.emailAddress)
                    .padding()
                Spacer()
                Button("Next") {
                    actionHandler(.next)
                }.padding()
            }
            Spacer()
        }
    }
    
    enum Actions {
        case next
    }
}

struct EmailView_Previews: PreviewProvider {
    static var previews: some View {
        EmailView { _ in
            
        }
    }
}

