import SwiftUI

struct PasswordView: View {
    let actionHandler: (Actions) -> Void
    @State var password: String = ""
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("Type your password")
                    .font(.title)
                    .padding()
                Spacer()
                TextField("password", text: $password)
                    .textContentType(.password)
                    .padding()
                Spacer()
                Button("Login") {
                    actionHandler(.login)
                }.padding()
            }
            Spacer()
        }
    }
    
    enum Actions {
        case login
    }
}

struct PasswordView_Previews: PreviewProvider {
    static var previews: some View {
        PasswordView { _ in
        }
    }
}
