import Foundation
import SwiftUI

class ChatListCoordinator: Coordinator {
    
    let context: UINavigationController
    let appStateManager: AppStateManager
    var messagesCoordinator: MessagesCoordinator?
    
    init(context: UINavigationController,
         appStateManager: AppStateManager = .init()) {
        self.context = context
        self.appStateManager = appStateManager
    }
    
    func start() {
        presentChatList()
    }
    
    func presentChatList() {
        let actionHandler = { [weak self] (action: ChatListView.Actions) in
            switch action {
            case .open(_):
                self?.presentMessages()
            case .logout:
                self?.appStateManager.logout()
            }
        }
        presentReplacing(view: ChatListView(actionHandler: actionHandler))
    }
    
    func presentMessages() {
        messagesCoordinator = .init(context: context)
        messagesCoordinator?.start()
    }
}
