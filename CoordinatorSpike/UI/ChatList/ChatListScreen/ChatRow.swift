import SwiftUI

struct ChatRow: View {
    let model: ChatModel
    let actionHandler: (ChatListView.Actions) -> Void
    
    var body: some View {
        VStack {
            HStack {
                Text("Person name").font(.headline)
                Spacer()
                Text("Date").font(.caption)
            }
            HStack {
                Text("Last message").font(.body)
                Spacer()
            }.padding(EdgeInsets(top: 16, leading: 0, bottom: 0, trailing: 0))
        }.padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0)).onTapGesture {
            actionHandler(.open(id: model.id))
        }
    }
}

struct ChatRow_Previews: PreviewProvider {
    static var previews: some View {
        ChatRow(model: .init(id: "1")) { _ in }
    }
}
