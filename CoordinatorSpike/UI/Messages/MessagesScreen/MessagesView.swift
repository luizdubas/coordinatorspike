import SwiftUI

struct MessagesView: View {
    let actionHandler: (Actions) -> Void
    
    var body: some View {
        VStack {
            Spacer()
            Text("Messages would appear here!")
                .padding()
            Spacer()
            Button("Close") {
                actionHandler(.close)
            }.padding()
        }
    }
    
    enum Actions {
        case close
    }
}

struct MessagesView_Previews: PreviewProvider {
    static var previews: some View {
        MessagesView { _ in }
    }
}
