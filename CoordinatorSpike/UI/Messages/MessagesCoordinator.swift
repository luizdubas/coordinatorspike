import Foundation
import SwiftUI

class MessagesCoordinator: Coordinator {
    
    let context: UINavigationController
    let newNavigationContext = UINavigationController()
    
    init(context: UINavigationController) {
        self.context = context
    }
    
    func start() {
        presentMessagesScreen()
    }
    
    func presentMessagesScreen() {
        let actionHandler = { [weak self] (action: MessagesView.Actions) in
            switch action {
            case .close:
                self?.context.dismiss(animated: true, completion: nil)
            }
        }
        presentModal(view: MessagesView(actionHandler: actionHandler), presentationStyle: .formSheet)
    }
}
